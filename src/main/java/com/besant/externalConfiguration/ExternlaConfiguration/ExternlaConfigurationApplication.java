package com.besant.externalConfiguration.ExternlaConfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExternlaConfigurationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExternlaConfigurationApplication.class, args);
	}

}
